# Braid Framework #

Braid framework is a simple, straightforward and explicit framework. No magic,
no annotations and very little dynamic structures. You need to define what you
want to do to make sure your web application does exactly what you want and only
what you want.

That being said, this framework hash been written for my own purposes and not
necessarily intended for serious usage. Use at your own peril.

## Credits ##

This library is copyright 2014 - 2015 to Riikka Kalliomäki.

See LICENSE for license and copying information.
