<?php

namespace Riimu\Braid\Application;

/**
 * @author Riikka Kalliomäki <riikka.kalliomaki@gmail.com>
 * @copyright Copyright (c) 2015, Riikka Kalliomäki
 * @license http://opensource.org/licenses/mit-license.php MIT License
 */
class RouteMatcherTest extends \PHPUnit_Framework_TestCase
{
    public function testSimplePaths()
    {
        $matcher = new RouteMatcher();
        $routes = [
            '/' => ['controller' => 'A', 'action' => 'action'],
            '/foo/' => ['controller' => 'B', 'action' => 'action'],
            '/foo/bar/' => ['controller' => 'C', 'action' => 'action'],
            '/baz/' => ['controller' => 'D', 'action' => 'action'],
        ];

        $this->assertRoute($routes['/'], $matcher->getMatchingPath($routes, '/'));
        $this->assertRoute($routes['/foo/'], $matcher->getMatchingPath($routes, '/foo/'));
        $this->assertRoute($routes['/foo/bar/'], $matcher->getMatchingPath($routes, '/foo/bar/'));
        $this->assertRoute($routes['/baz/'], $matcher->getMatchingPath($routes, '/baz/'));
    }

    public function testFailedRoute()
    {
        $matcher = new RouteMatcher();
        $routes = [
            '/' => ['controller' => 'A', 'action' => 'action'],
        ];

        $this->assertNull($matcher->getMatchingPath($routes, '/foo/'));
    }

    public function testRouteMatching()
    {
        $matcher = new RouteMatcher();
        $routes = [
            '/foo/' => ['controller' => 'A', 'action' => 'action'],
        ];

        $this->assertNull($matcher->getMatchingRoute($routes, '/', ['foo']));
        $this->assertRoute($routes['/foo/'], $matcher->getMatchingRoute($routes, '/foo/', []));
    }

    public function testSlashHandling()
    {
        $matcher = new RouteMatcher();
        $routes = [
            '/foo/bar/' => ['controller' => 'A', 'action' => 'action'],
        ];

        $this->assertRoute($routes['/foo/bar/'], $matcher->getMatchingPath($routes, 'foo/bar/'));
        $this->assertRoute($routes['/foo/bar/'], $matcher->getMatchingPath($routes, '/foo/bar'));
        $this->assertRoute($routes['/foo/bar/'], $matcher->getMatchingPath($routes, 'foo/bar'));
    }

    public function testParamCountMatch()
    {
        $matcher = new RouteMatcher();
        $routes = [
            '/' => ['controller' => 'A', 'action' => 'action', 'params' => 2],
            '/foo/' => ['controller' => 'B', 'action' => 'action', 'params' => 2],
        ];

        $this->assertNull($matcher->getMatchingPath($routes, '/foo/'));
        $this->assertRoute($routes['/'], $matcher->getMatchingPath($routes, '/foo/bar/'));
        $this->assertRoute($routes['/foo/'], $matcher->getMatchingPath($routes, '/foo/bar/baz/'));
    }

    public function testReturnedRoute()
    {
        $matcher = new RouteMatcher();
        $routes = [
            '/foo/' => ['controller' => 'A', 'action' => 'action', 'params' => 2],
        ];

        $this->assertSame(
            [
                'route' => '/foo/',
                'path' => '/foo/bar/baz/',
                'values' => ['bar', 'baz'],
                'controller' => 'A',
                'action' => 'action',
                'params' => 2
            ],
            $matcher->getMatchingPath($routes, '/foo/bar/baz/')
        );
    }

    public function testSlashOmission()
    {
        $matcher = new RouteMatcher();
        $routes = [
            '/foo/' => ['controller' => 'A', 'action' => 'action', 'omitSlash' => true],
            '/bar/' => ['controller' => 'B', 'action' => 'action', 'omitSlash' => true, 'params' => 1],
            '/baz/' => ['controller' => 'C', 'action' => 'action'],
        ];

        $this->assertSame('/foo', $matcher->getMatchingPath($routes, '/foo/')['path']);
        $this->assertSame('/bar/baz', $matcher->getMatchingPath($routes, '/bar/baz/')['path']);
        $this->assertSame('/baz/', $matcher->getMatchingPath($routes, '/baz/')['path']);
    }

    public function testMultipleDefinitions()
    {
        $matcher = new RouteMatcher();
        $routes = [
            '/foo/' => [
                ['controller' => 'A', 'action' => 'action', 'params' => 1],
                ['controller' => 'B', 'action' => 'action', 'params' => 2],
            ]
        ];

        $this->assertNull($matcher->getMatchingPath($routes, '/foo/'));
        $this->assertRoute($routes['/foo/'][0], $matcher->getMatchingPath($routes, '/foo/bar/'));
        $this->assertRoute($routes['/foo/'][1], $matcher->getMatchingPath($routes, '/foo/bar/baz/'));
    }

    public function testMethodSynonyms()
    {
        $matcher = new RouteMatcher();
        $routes = [
            '/foo/' => ['controller' => 'A', 'action' => 'action', 'method' => 'get'],
            '/bar/' => ['controller' => 'B', 'action' => 'action', 'method' => 'head'],
        ];

        $this->assertRoute($routes['/foo/'], $matcher->getMatchingPath($routes, '/foo/', 'get'));
        $this->assertRoute($routes['/foo/'], $matcher->getMatchingPath($routes, '/foo/', 'head'));
        $this->assertRoute($routes['/bar/'], $matcher->getMatchingPath($routes, '/bar/', 'get'));
        $this->assertRoute($routes['/bar/'], $matcher->getMatchingPath($routes, '/bar/', 'head'));
    }

    public function testMethodCaseSensitivity()
    {
        $matcher = new RouteMatcher();
        $routes = [
            '/foo/' => ['controller' => 'A', 'action' => 'action', 'method' => 'get'],
            '/bar/' => ['controller' => 'B', 'action' => 'action', 'method' => 'GET'],
        ];

        $this->assertRoute($routes['/foo/'], $matcher->getMatchingPath($routes, '/foo/', 'GET'));
        $this->assertRoute($routes['/bar/'], $matcher->getMatchingPath($routes, '/bar/', 'get'));
    }

    public function testIncorrectMethod()
    {
        $matcher = new RouteMatcher();
        $routes = [
            '/' => ['controller' => 'A', 'action' => 'action'],
            '/foo/' => ['controller' => 'B', 'action' => 'action', 'method' => 'post'],
        ];

        $this->assertNull($matcher->getMatchingPath($routes, '/', 'post'));
        $this->assertNull($matcher->getMatchingPath($routes, '/foo/', 'get'));
    }

    public function testMultipleMethods()
    {
        $matcher = new RouteMatcher();
        $routes = [
            '/' => ['controller' => 'A', 'action' => 'action', 'method' => ['get', 'post']],
        ];

        $this->assertRoute($routes['/'], $matcher->getMatchingPath($routes, '/', 'get'));
        $this->assertRoute($routes['/'], $matcher->getMatchingPath($routes, '/', 'post'));
    }

    public function testParamStringMatch()
    {
        $matcher = new RouteMatcher();
        $routes = [
            '/' => ['controller' => 'A', 'action' => 'action', 'params' => ['foo']],
        ];

        $this->assertNull($matcher->getMatchingPath($routes, '/'));
        $this->assertNull($matcher->getMatchingPath($routes, '/bar/'));
        $this->assertNull($matcher->getMatchingPath($routes, '/foo/bar/'));

        $this->assertRoute($routes['/'], $matcher->getMatchingPath($routes, '/foo/'));
    }

    public function testParamStringMatchType()
    {
        $matcher = new RouteMatcher();
        $routes = [
            '/' => ['controller' => 'A', 'action' => 'action', 'params' => ['100']],
        ];

        $this->assertNull($matcher->getMatchingRoute($routes, '/', [1]));
        $this->assertRoute($routes['/'], $matcher->getMatchingRoute($routes, '/', [100]));
    }

    public function testRegexpMatching()
    {
        $matcher = new RouteMatcher();
        $routes = [
            '/' => ['controller' => 'A', 'action' => 'action', 'params' => ['/b.{2}/']],
        ];

        $this->assertNull($matcher->getMatchingPath($routes, '/foo/'));
        $this->assertNull($matcher->getMatchingPath($routes, '/foobar/'));
        $this->assertRoute($routes['/'], $matcher->getMatchingPath($routes, '/bar/'));
    }

    public function testParameterSet()
    {
        $matcher = new RouteMatcher();
        $routes = [
            '/' => ['controller' => 'A', 'action' => 'action', 'params' => ['/.*/', 'profile'], 'paramSet' => [0]],
        ];

        $this->assertSame(['user'], $matcher->getMatchingPath($routes, '/user/profile/')['values']);
    }

    public function testNamedParameters()
    {
        $matcher = new RouteMatcher();
        $routes = [
            '/' => [
                'controller' => 'A',
                'action' => 'action',
                'params' => ['feed', '/(?P<name>[^.]*)\.(?P<format>rss|atom)/'],
                'paramSet' => ['name', 'format']
            ],
        ];

        $this->assertSame(['john', 'rss'], $matcher->getMatchingPath($routes, '/feed/john.rss')['values']);
    }

    public function testInvalidParameterName()
    {
        $matcher = new RouteMatcher();
        $routes = [
            '/' => ['controller' => 'A', 'action' => 'action', 'params' => ['/.*/', 'profile'], 'paramSet' => ['user']],
        ];

        $this->setExpectedException('\RuntimeException');
        $matcher->getMatchingPath($routes, '/user/profile/');
    }

    // Makes sure that parameter names are only set from the matching route
    public function testNamedParameterBleedBug()
    {
        $matcher = new RouteMatcher();
        $routes = [
            '/' => [
                [
                    'controller' => 'A',
                    'action' => 'action',
                    'params' => ['/(?P<foo>.*)/', 'foo'],
                ],
                [
                    'controller' => 'B',
                    'action' => 'action',
                    'params' => ['/(?P<bar>.*)/', 'bar'],
                    'paramSet' => ['foo'],
                ]
            ],
        ];

        $this->setExpectedException('\RuntimeException', 'Invalid named route parameter \'foo\'');
        $matcher->getMatchingPath($routes, '/value/bar/');
    }

    public function testAlias()
    {
        $matcher = new RouteMatcher();
        $routes = [
            '/foo/' => ['alias' => '/bar/$1.$0', 'params' => 2],
            '/bar/' => ['controller' => 'A', 'action' => 'action', 'params' => ['/[^.]*\.rss/']]
        ];

        $this->assertRoute($routes['/bar/'], $matcher->getMatchingPath($routes, '/foo/rss/user/'));
    }

    public function testInvalidAliasParameter()
    {
        $matcher = new RouteMatcher();
        $routes = [
            '/foo/' => ['alias' => '/bar/$2.$1', 'params' => 2],
            '/bar/' => ['controller' => 'A', 'action' => 'action', 'params' => ['/[^.]*\.rss/']]
        ];

        $this->setExpectedException('\RuntimeException');
        $matcher->getMatchingPath($routes, '/foo/rss/user/');
    }

    private function assertRoute(array $expected, $actual)
    {
        $this->assertInternalType('array', $actual);
        $this->assertSame($expected, array_intersect_key($actual, $expected));
    }
}
