<?php

namespace Riimu\Braid\Application;

/**
 * Exception that is thrown when the router attempts to return a path to an invalid route.
 * @author Riikka Kalliomäki <riikka.kalliomaki@gmail.com>
 * @copyright Copyright (c) 2015, Riikka Kalliomäki
 * @license http://opensource.org/licenses/mit-license.php MIT License
 */
class InvalidRouteException extends \Exception
{

}
