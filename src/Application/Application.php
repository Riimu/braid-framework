<?php

namespace Riimu\Braid\Application;

/**
 * Abstract application class that handles routing and controller dispatching.
 * @author Riikka Kalliomäki <riikka.kalliomaki@gmail.com>
 * @copyright Copyright (c) 2014, Riikka Kalliomäki
 * @license http://opensource.org/licenses/mit-license.php MIT License
 */
abstract class Application
{
    /**
     * Runs the application.
     * @return mixed The application result status.
     */
    public function run()
    {
        return $this->processRequest();
    }

    /**
     * Processes the application request.
     * @return mixed The application result status.
     * @throws RouteNotFoundException If the requested route was not found
     */
    protected function processRequest()
    {
        $this->initialize();
        $route = $this->getRouter()->getRequestRoute();

        if (isset($route['controller'], $route['action'], $route['values'])) {
            $controller = $this->setUp($route['controller']);
            $result = $this->dispatch($controller, $route['action'], $route['values']);
            return $this->tearDown($controller, $result);
        }

        throw new \RuntimeException("Invalid request route was returned");
    }

    /**
     * Initializes the application.
     */
    protected function initialize()
    {

    }

    /**
     * Returns the application router used to route requests.
     * @return Router The application router
     */
    abstract protected function getRouter();

    /**
     * Creates and initializes the route controller.
     * @param string $class Name of the controller class
     * @return Controller The route controller
     */
    protected function setUp($class)
    {
        if (!class_exists($class)) {
            throw new \RuntimeException("The route controller '$class' does not exist");
        }

        $controller = new $class;

        if ($controller instanceof Controller) {
            $controller->setRouter($this->getRouter());
            return $controller;
        }

        throw new \RuntimeException("The route controller '$class' is not a valid controller");
    }

    /**
     * Runs the action sequence for the route controller.
     * @param Controller $controller The route controller
     * @param string $action Name of the action to run
     * @param array $params Parameters passed to the action
     * @return mixed The result value returned by the controller `tearDown()`
     */
    protected function dispatch(Controller $controller, $action, array $params)
    {
        if (!method_exists($controller, $action)) {
            throw new \RuntimeException(
                sprintf("The route controller '%s' does not define the method '%s'", get_class($controller), $action)
            );
        }

        $controller->setUp();
        $result = call_user_func_array([$controller, $action], $params);
        return $controller->tearDown($result);
    }

    /**
     * Tears down the application.
     * @param Controller $controller The route controller
     * @param mixed $result The result value from the controller dispatch
     * @return mixed The application result status.
     */
    protected function tearDown(Controller $controller, $result)
    {
        return 0;
    }
}
