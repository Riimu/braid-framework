<?php

namespace Riimu\Braid\Application;

/**
 * Handles matching paths and parameters to specific routes.
 * @author Riikka Kalliomäki <riikka.kalliomaki@gmail.com>
 * @copyright Copyright (c) 2015, Riikka Kalliomäki
 * @license http://opensource.org/licenses/mit-license.php MIT License
 */
class RouteMatcher
{
    /**
     * Returns the route that matches the given path.
     * @param array $routes The configured application routes
     * @param string $path The path to match
     * @param string|null $method The request method or null to ignore
     * @return array|null The matching route or null if none
     */
    public function getMatchingPath(array $routes, $path, $method = null)
    {
        $parts = $this->getPathParts($path);

        for ($i = count($parts); $i >= 0; $i--) {
            $path = implode('/', array_slice($parts, 0, $i));
            $params = array_slice($parts, $i);

            if ($route = $this->getMatchingRoute($routes, $path, $params, $method)) {
                return $route;
            }
        }

        return null;
    }

    /**
     * Returns the route that matches the given route path and parameters.
     * @param array $routes The configured application routes
     * @param string $routePath The route path to match
     * @param string[] $params The route parameters to match
     * @param string|null $method The request method or null to ignore
     * @return array|null The matching route or null if none
     */
    public function getMatchingRoute(array $routes, $routePath, array $params, $method = null)
    {
        $params = array_map('strval', $params);
        $path = $this->canonizePath($routePath);

        if (!isset($routes[$path])) {
            return null;
        }

        if ($route = $this->getMatch($routes[$path], $params, $method)) {
            if (isset($route['alias'])) {
                return $this->getAlias($routes, $route, $method);
            }

            return [
                'route' => $path,
                'path' => rtrim($path . implode('/', $params), '/') . str_repeat('/', empty($route['omitSlash'])),
            ] + $route;
        }

        return null;
    }

    /**
     * Returns the aliased route.
     * @param array $routes The configured application routes
     * @param array $route The matched route that contains an alias
     * @param string|null $method The request method or null to ignore
     * @return array|null The matching route or null if none
     */
    private function getAlias(array $routes, array $route, $method)
    {
        $path = preg_replace_callback('/\\$(\d+)/', function ($match) use ($route) {
            if (!isset($route['values'][$match[1]])) {
                throw new \RuntimeException("Invalid alias parameter number '$match[1]'");
            }

            return $route['values'][$match[1]];
        }, $route['alias']);

        return $this->getMatchingPath($routes, $path, $method);
    }

    /**
     * Canonizes the forward slashes in the path.
     * @param string $path The path to canonize
     * @return string The canonized path
     */
    public function canonizePath($path)
    {
        $parts = $this->getPathParts($path);
        return $parts === [] ? '/' : sprintf('/%s/', implode('/', $parts));
    }

    /**
     * Returns the individual nonempty parts from the path.
     * @param string $path The path to split into parts
     * @return array The nonempty part from the path
     */
    private function getPathParts($path)
    {
        return array_filter(explode('/', $path), function ($value) {
            return $value !== '';
        });
    }

    /**
     * Returns the matching route from the route definitions.
     * @param array $routes Single route definition or array of route definitions
     * @param string[] $params The parameters to match
     * @param string|null $method The request method or null to ignore
     * @return array|null The matching route or null if none
     */
    private function getMatch(array $routes, array $params, $method)
    {
        if (array_filter(array_keys($routes), 'is_string') !== []) {
            $routes = [$routes];
        }

        foreach ($routes as $route) {
            if ($this->matchRoute($route, $params, $method)) {
                return ['values' => $this->processParams($route, $params)] + $route;
            }
        }

        return null;
    }

    /**
     * Returns the route parameter values based on the parsed parameter values.
     * @param array $route The route definition
     * @param string[] $params The parsed route parameters values
     * @return array The parameter values for the route
     */
    private function processParams(array $route, array $params)
    {
        if (!isset($route['paramSet'])) {
            return array_intersect_key($params, array_flip(array_filter(array_keys($params), 'is_int')));
        }

        $values = [];

        foreach ($route['paramSet'] as $name) {
            if (!isset($params[$name])) {
                throw new \RuntimeException("Invalid named route parameter '$name'");
            }

            $values[] = $params[$name];
        }

        return $values;
    }

    /**
     * Tells if the parameters match against the route definition the parameters against
     * @param array $route The route definition
     * @param string[] $params The parameters to match
     * @param string|null $method The request method or null to ignore
     * @return bool True if the route matches, false if not
     */
    private function matchRoute(array $route, array & $params, $method)
    {
        $route += ['params' => 0, 'method' => ['get', 'head']];

        if (!$this->matchCount($route, $params)) {
            return false;
        } elseif (!$this->matchMethod($route, $method)) {
            return false;
        } elseif (!$this->matchValues($route, $params)) {
            return false;
        }

        return true;
    }

    /**
     * Tells if the parameter count matches the expected number of parameters.
     * @param array $route The route definition
     * @param string[] $params The parameters to match
     * @return bool True if the parameter count is correct, false if not
     */
    private function matchCount(array $route, array $params)
    {
        $expected = is_array($route['params']) ? count($route['params']) : (int) $route['params'];
        return count($params) === $expected;
    }

    /**
     * Tells if the request method is one of the allowed methods.
     * @param array $route The route definition
     * @param string|null $method The request method or null to ignore
     * @return bool True if the request method is allowed, false if not
     */
    private function matchMethod(array $route, $method)
    {
        if ($method === null) {
            return true;
        }

        $methods = is_array($route['method']) ? $route['method'] : [(string) $route['method']];
        $methods = array_map('strtolower', $methods);
        $synonyms = array_diff(['get', 'head'], $methods);

        if (count($synonyms) === 1) {
            $methods[] = array_pop($synonyms);
        }

        return in_array(strtolower($method), $methods);
    }

    /**
     * Tells if the parameter values match and sets the parsed parameter values.
     * @param array $route The route definition
     * @param string[] $params The parameters to match
     * @return bool True values match, false if not
     */
    private function matchValues(array $route, & $params)
    {
        if (!is_array($route['params'])) {
            return true;
        }

        $values = [];

        foreach (array_values($params) as $param) {
            $parsed = $this->getParsedValues($param, array_shift($route['params']));
            if ($parsed === false) {
                return false;
            }
            $values = $parsed + $values;
        }

        $params += $values;
        return true;
    }

    /**
     * Returns the values parsed from the parameter.
     * @param string $param The parameter to parse
     * @param string $rule The parsing pattern or string to match
     * @return array|bool
     */
    private function getParsedValues($param, $rule)
    {
        $expected = (string) $rule;
        $values = [];

        if (substr($rule, 0, 1) === '/') {
            if (!preg_match($rule, $param, $match)) {
                return false;
            }

            $expected = (string) $match[0];
            $values = array_diff_key($match, array_flip(array_filter(array_keys($match), 'is_int')));
        }

        return $expected === (string) $param ? $values : false;
    }
}
