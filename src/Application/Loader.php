<?php

namespace Riimu\Braid\Application;

/**
 * Interface for generic dependency loaders.
 * @author Riikka Kalliomäki <riikka.kalliomaki@gmail.com>
 * @copyright Copyright (c) 2015, Riikka Kalliomäki
 * @license http://opensource.org/licenses/mit-license.php MIT License
 */
interface Loader
{
    /**
     * Returns all the dependencies handled by this loader.
     * @return string[] Associative array of dependencies and the loading methods
     */
    public function getDependencies();
}
