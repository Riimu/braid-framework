<?php

namespace Riimu\Braid\Application;

use Riimu\Braid\View\View;

/**
 * Abstract controller class that handles setting up and rendering the view.
 * @author Riikka Kalliomäki <riikka.kalliomaki@gmail.com>
 * @copyright Copyright (c) 2014, Riikka Kalliomäki
 * @license http://opensource.org/licenses/mit-license.php MIT License
 */
abstract class Controller
{
    /** @var \Riimu\Braid\Application\Router Application router */
    protected $router;

    /** @var \Riimu\Braid\View\View Controller view */
    protected $view;

    /**
     * Sets the application router.
     * @param Router $router The application router
     */
    public function setRouter(Router $router)
    {
        $this->router = $router;
    }

    /**
     * Sets up the route controller.
     */
    public function setUp()
    {
        $this->view = new View();
        $this->view->link = $this->router;
    }

    /**
     * Tears down the route controller.
     * @param mixed $result The value returned from the controller action
     * @return mixed The result value passed to the application
     */
    public function tearDown($result)
    {
        if ($result !== false && !headers_sent()) {
            $this->view->render();
        }

        return true;
    }
}
