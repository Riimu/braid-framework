<?php

namespace Riimu\Braid\Application;

/**
 * Loads and stores the dependencies for the application.
 * @author Riikka Kalliomäki <riikka.kalliomaki@gmail.com>
 * @copyright Copyright (c) 2014, Riikka Kalliomäki
 * @license http://opensource.org/licenses/mit-license.php MIT License
 */
abstract class Container
{
    /** @var array The list of defined dependencies */
    private $dependencies;

    /** @var array The list of loaded dependencies */
    private $loaded;

    /**
     * Creates a new instance of Container.
     * @param array $dependencies The list of defined dependencies
     */
    public function __construct(array $dependencies)
    {
        $this->dependencies = array_change_key_case($dependencies);
        $this->loaded = [];
    }

    /**
     * Sets a new dependency.
     * @param string $dependency Name of the dependency
     * @param mixed $definition The dependency definition
     */
    public function set($dependency, $definition)
    {
        $dependency = strtolower($dependency);

        if (array_key_exists($dependency, $this->loaded)) {
            unset($this->loaded[$dependency]);
        }

        $this->dependencies[$dependency] = $definition;
    }

    /**
     * adds the dependencies from the dependency loader.
     * @param Loader $loader The dependency loader
     */
    public function addLoader(Loader $loader)
    {
        foreach ($loader->getDependencies() as $name => $method) {
            $this->set($name, [$loader, $method]);
        }
    }

    /**
     * Returns the application dependency.
     * @param string $dependency Name of the dependency
     * @return mixed The value for the dependency
     * @throws \InvalidArgumentException If attempting to get an object dependency
     */
    public function get($dependency)
    {
        $value = $this->getValue($dependency);

        if (is_object($value)) {
            throw new \InvalidArgumentException("Cannot get object dependency '$dependency' via generic getter");
        }

        return $value;
    }

    /**
     * Returns an instance of the application dependency.
     * @param string $dependency Name of the dependency
     * @return object Instance of the dependency
     * @throws \InvalidArgumentException If the dependency cannot be instantiated
     */
    protected function getInstance($dependency)
    {
        $value = $this->getValue($dependency);

        if (!is_object($value)) {
            throw new \InvalidArgumentException("Cannot generate instance of dependency '$dependency'");
        }

        return $value;
    }

    /**
     * Returns the value of dependency and loads it, if it has not yet been loaded.
     * @param string $dependency Name of the dependency
     * @return mixed The value for the dependency
     */
    private function getValue($dependency)
    {
        $dependency = strtolower($dependency);

        if (!array_key_exists($dependency, $this->loaded)) {
            $this->loaded[$dependency] = $this->getNew($dependency);
        }

        return $this->loaded[$dependency];
    }

    /**
     * Returns the loaded value for the dependency.
     * @param string $dependency Name of the dependency
     * @return mixed The value for the dependency
     */
    private function getNew($dependency)
    {
        if (!isset($this->dependencies[$dependency])) {
            throw new \InvalidArgumentException("Unknown dependency '$dependency'");
        }

        if (is_string($this->dependencies[$dependency]) || !is_callable($this->dependencies[$dependency])) {
            return $this->dependencies[$dependency];
        }

        return call_user_func($this->dependencies[$dependency], $this);
    }
}
