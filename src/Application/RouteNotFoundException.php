<?php

namespace Riimu\Braid\Application;

/**
 * Exception that is thrown when the requested route is not found.
 * @author Riikka Kalliomäki <riikka.kalliomaki@gmail.com>
 * @copyright Copyright (c) 2014, Riikka Kalliomäki
 * @license http://opensource.org/licenses/mit-license.php MIT License
 */
class RouteNotFoundException extends \Exception
{

}
