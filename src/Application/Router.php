<?php

namespace Riimu\Braid\Application;

/**
 * Handles returning requested route and generating application paths.
 * @author Riikka Kalliomäki <riikka.kalliomaki@gmail.com>
 * @copyright Copyright (c) 2014, Riikka Kalliomäki
 * @license http://opensource.org/licenses/mit-license.php MIT License
 */
class Router
{
    /** @var string The scheme and hostname for the application */
    private $url;

    /** @var string The application base url path */
    private $path;

    /** @var array The configured application routes */
    private $routes;

    /** @var string The path to the current application request */
    private $self;

    /** @var RouteMatcher The matcher used to find application routes */
    private $matcher;

    /**
     * Creates a new instance of Router.
     * @param array $routes The configured application routes
     */
    public function __construct(array $routes)
    {
        $host = $this->getEnv('SERVER_NAME', null);
        $https = $this->getEnv('HTTPS', false);
        $path = $this->getEnv('SCRIPT_NAME', '/');

        if (empty($host)) {
            throw new \RuntimeException("Cannot determine domain name");
        }

        $this->url = sprintf('%s://%s', empty($https) ? 'http' : 'https', $host);
        $this->path = substr($path, 0, strrpos($path, '/'));
        $this->routes = $routes;
        $this->self = '';
        $this->matcher = new RouteMatcher();
    }

    /**
     * Returns a server environment variable.
     * @param string $property Name of the environment variable
     * @param mixed $default Default value if the variable does not exist
     * @return mixed The value for the environment variable
     */
    protected function getEnv($property, $default)
    {
        return isset($_SERVER[$property]) ? $_SERVER[$property] : $default;
    }

    /**
     * Returns the requested application route.
     *
     * This method parses the requested path from REQUEST_URI and returns the
     * appropriate route. If the route points to a different canonical path than
     * the one requested, a redirection header will be sent to the canonical
     * path. This method will also parse the GET variables from the REQUEST_URI
     * and populate the $_GET global array.
     *
     * @return array The requested application route
     * @throws RouteNotFoundException If the requested application route could not be found
     */
    public function getRequestRoute()
    {
        $uri = $this->getEnv('REQUEST_URI', '');
        $method = $this->getEnv('REQUEST_METHOD', 'GET');
        $path = substr(urldecode((string) parse_url($uri, PHP_URL_PATH)), strlen($this->path));
        $route = $this->matcher->getMatchingPath($this->routes, $path, $method);

        if ($route === null) {
            throw new RouteNotFoundException("No route found for path '$path'");
        }

        parse_str((string) parse_url($uri, PHP_URL_QUERY), $_GET);

        if (in_array(strtolower($method), ['get', 'head']) && $route['path'] !== '/' . ltrim($path, '/')) {
            $this->redirect($route['path'], [], $_GET);
        }

        $this->self = $route['path'];

        return $route;
    }

    /**
     * Sends a redirection header and kills the script.
     * @param string $path The application path
     * @param array $params Application path parameters
     * @param array $get GET variables for the application path
     */
    public function redirect($path, array $params = [], array $get = [])
    {
        header('Location: ' . $this->url($path, $params, $get), true, 302);
        exit;
    }

    /**
     * Returns the absolute url to the application path.
     * @param string $path The application path
     * @param array $params Application path parameters
     * @param array $get GET variables for the application path
     * @return string The absolute url to the application path
     */
    public function url($path, array $params = [], array $get = [])
    {
        return $this->url . $this->path($path, $params, $get);
    }

    /**
     * Returns the absolute path to the application path.
     * @param string $path The application path
     * @param array $params Application path parameters
     * @param array $get GET variables for the application path
     * @return string The absolute path to the application path
     */
    public function path($path, array $params = [], array $get = [])
    {
        $parts = array_filter(
            array_merge(explode('/', $this->path . '/' . $path), $params),
            function ($value) {
                return (string) $value !== '';
            }
        );

        $full = $parts === [] ? '/' : sprintf('/%s/', implode('/', array_map('urlencode', $parts)));

        if (substr($path, -1) !== '/') {
            $full = substr($full, 0, -1);
        }

        return $this->addQuery($full, $get);
    }

    /**
     * Returns the canonical path to the application route.
     * @param string $path The application route path
     * @param array $params Application route parameters
     * @param array $get GET variables for the path
     * @return string The canonical absolute path to the application route
     * @throws InvalidRouteException If the path is not a valid application route
     */
    public function route($path, array $params = [], array $get = [])
    {
        $route = $this->matcher->getMatchingRoute($this->routes, $path, $params);

        if ($route === null) {
            throw new InvalidRouteException(sprintf(
                "The path '%s' is not valid application route with params: %s",
                $path,
                implode(', ', $params)
            ));
        }

        return $this->path($route['path'], [], $get);
    }

    /**
     * Adds the query part to the url path.
     * @param string $path The url path to modify
     * @param array $get The GET parameters to add to the path
     * @return string The path with the get parameters added
     */
    private function addQuery($path, array $get)
    {
        if ($get === []) {
            return $path;
        }

        return $path . '?' . http_build_query($get, '', '&', PHP_QUERY_RFC3986);
    }

    /**
     * Returns the path to the current application request
     * @param bool $url True to return as an url, false to return as a path
     * @param array $get GET variables for the path
     * @return string The absolute url or path to the current request
     */
    public function self($url = false, array $get = [])
    {
        if ($url === true) {
            return $this->url($this->self, [], $get);
        }

        return $this->path($this->self, [], $get);
    }
}
